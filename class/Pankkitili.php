<?php
class Pankkitili {
  private $saldo;
  private $tilinumero;
  private $tietokanta;

  public function __construct() {
    try {
      $this->tietokanta = new PDO('mysql:host=localhost;dbname=verkkopankki;charset=utf8','root','root1234');
      $this->tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $pdoex) {
      throw new Exception('Häiriö tietokantayhteydessä!');
    }

    $sql ="select * from pankkitili where id = 1";
    $kysely = $this->tietokanta->query($sql);
    $tietue = $kysely->fetch();
    $this->saldo = $tietue['saldo'];
    $this->tilinumero = $tietue['tilinumero'];
  }

  public function naytaSaldo(): float {
    return $this->saldo;
  }

  public function talleta(float $summa): void {
    $this->saldo = $this->saldo + $summa;
    $this->paivitaSaldo();
  }

  public function nosta(float $summa): void {
    if ($this->saldo >= $summa) {
      $this->saldo = $this->saldo - $summa;
      $this->paivitaSaldo();
    }
  }

  private function paivitaSaldo(): void  {
    try {
      $sql ="update pankkitili set saldo=" . $this->saldo;
      $this->tietokanta->query($sql);
    }
    catch (PDOException $pdoex) {
      throw new Exception('Virhe saldon päivityksessä!');
    }
  }
}