<?php
require_once 'class/Pankkitili.php';
$pankkitili = new Pankkitili();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verkkopankki</title>
</head>
<body>
  <h3>Verkkopankki</h3>
  <p>Verkkopankin saldo on <?php print $pankkitili->naytaSaldo();?></p>
  <form action="talleta.php" method="post">
  <p>Talleta rahaa</p>
  <label>Summa</label>
  <input name="summa" type="number">
  <button>Talleta</button>
  </form>
  <form action="nosta.php" method="post">
  <p>Nosta rahaa</p>
  <label>Summa</label>
  <input name="summa" type="number">
  <button>Nosta</button>
  </form>
</body>
</html>