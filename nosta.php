<?php
require_once 'class/Pankkitili.php';
$pankkitili = new Pankkitili();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verkkopankki</title>
  <?php
  $summa = filter_input(INPUT_POST,'summa',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
  $pankkitili->nosta($summa);
  print '<p>Tilillä on rahaa ' . $pankkitili->naytaSaldo() . '</p>';
  ?>
  <a href="index.php">Etusivulle</a>
</head>
<body>
  
</body>
</html>