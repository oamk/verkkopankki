create database verkkopankki;
use verkkopankki;

create table pankkitili (
  id int primary key auto_increment,
  saldo decimal(10,2) not null,
  tilinumero varchar(18) not null
);

insert into pankkitili (saldo, tilinumero) values (1000,'FI1122223333444455');
